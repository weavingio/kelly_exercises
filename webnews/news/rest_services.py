from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.urls import path
from django.conf.urls import url
from .models import Article, Reporter


from .serializers import ArticleSerializer
from .serializers import ReporterSerializer

@api_view(['GET'])
def api_get_first_article(request):
    obj = Article.objects.first()

    if obj:
    	serializer = ArticleSerializer(obj)
    	return Response(serializer.data)
    else:
    	return Response({"Message": 'Article Not Foud'})

@api_view(['GET'])
def api_all_news(request):
    all_articles = Article.objects.all()
    if all_articles:
    	serializer = ArticleSerializer(all_articles, many=True)
    	return Response(serializer.data)
    else:
    	return Response({"Message": 'Article Not Foud'})


@api_view(['POST'])
def api_add_article(request):
    heading = request.POST.get("heading", None)
    body = request.POST.get("body", None)
    reporter_id = request.POST.get("reporter_id", None)
    article = Article.objects.create(heading=heading,
                                     body=body,
                                     reporter_id = reporter_id,
                                     image=request.FILES['image'])

    return Response({'message': 'article {:d} created'.format(article.id)}, status=301)


urlpatterns = [
    #url(r'^(?P<_name>[A-Za-z%]+)/$', e),
    path('first/', api_get_first_article),
    path('all/', api_all_news),
    path('add/', api_add_article),
    #url(r'^(?P<_range>[0-9,-]+)',get_articles_range),
]
