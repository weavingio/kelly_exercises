from rest_framework import serializers
from .models import Article, Reporter

class ReporterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reporter
        fields = ('id', 'name', 'n_articles')

class ArticleSerializer(serializers.ModelSerializer):
    #reporter = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta:
        model = Article
        fields = ('id', 'heading', 'body', 'image', 'created', 'reporter')
        depth = 2
