
from django.urls import path
from django.conf.urls import url
from .views import greet, get_first_article
from .views import all_news, get_articles_reporter
from .views import get_article_id, get_articles_range
from .rest_services import api_get_first_article, api_all_news


urlpatterns = [
    path('greet/', greet),
    path('first/', get_first_article),
    path('all/', all_news),
    url(r'^(?P<_name>[A-Za-z%]+)/$', get_articles_reporter),
    url(r'^(?P<_id>\d+)$', get_article_id),
    url(r'^(?P<_range>[0-9,-]+)',get_articles_range),
]
