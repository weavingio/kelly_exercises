from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from django.db.models import Q

# Create your views here.
from .models import Article

def greet(request):
    return HttpResponse("<H1> Hello Students!</H1>")

def get_first_article(request):
    object = Article.objects.first()
    if object:
        context = {
            'all_articles': [object]
        }
        return render(request, 'frontpage.html', context)
    else:
        return HttpResponseNotFound("Article not found")

def all_news(request):
    all_articles = Article.objects.all()
    if all_articles:
        context = {
            'all_articles': all_articles
        }
        return render(request, 'frontpage.html', context)
    else:
        return HttpResponseNotFound("Article not found")


def get_articles_reporter(request, _name):
    _name = _name.replace('%', ' ')
    all_articles = Article.objects.filter(reporter__name = _name)
    if all_articles:
        context = {
            'all_articles': all_articles
        }
        return render(request, 'frontpage.html', context)
    else:
        return HttpResponseNotFound("Article not found")



def get_article_id(request, _id):

    all_articles = Article.objects.filter(id = _id)
    if all_articles:
        context = {
            'all_articles': all_articles
        }
        return render(request, 'frontpage.html', context)
    else:
        return HttpResponseNotFound("Article not found")



def get_articles_range(request, _range):
    print('===================>',_range)

    if ',' in _range:
        ids = [int(x) for x in _range.split(',')]
        all_articles = Article.objects.filter(id__in=ids)

    else:
        start, end = [int(x) for x in _range.split('-')]
        all_articles = Article.objects.filter(Q(id__lte = end) & Q(id__gte = start))


    if all_articles:
        context = {
            'all_articles': all_articles
        }
        return render(request, 'frontpage.html', context)
    else:
        return HttpResponseNotFound("Article not found")
